FROM golang:1.19-alpine as builder
WORKDIR /app
COPY . .
RUN go build -o google-cloud-auth

# Start a new stage to create a smaller image
FROM alpine:3.14
WORKDIR /app
RUN apk update && apk add bash curl
COPY --from=builder /app/google-cloud-auth /usr/bin/google-cloud-auth
CMD ["google-cloud-auth"]
