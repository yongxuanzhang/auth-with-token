package auth

type Format struct {
	Type string `json:"type"`
}

type CredentialSource struct {
	File   string `json:"file"`
	Format Format `json:"format"`
}

type ServiceAccountImpersonation struct {
	TokenLifetimeSeconds int `json:"token_lifetime_seconds"`
}

type ExternalAccountConfig struct {
	Type                           string           `json:"type"`
	Audience                       string           `json:"audience"`
	SubjectTokenType               string           `json:"subject_token_type"`
	TokenURL                       string           `json:"token_url"`
	CredentialSource               CredentialSource `json:"credential_source"`
	ServiceAccountImpersonationURL string           `json:"service_account_impersonation_url"`
	//ServiceAccountImpersonation    ServiceAccountImpersonation `json:"service_account_impersonation"`
}
