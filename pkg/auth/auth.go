package auth

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"

	artifactregistry "cloud.google.com/go/artifactregistry/apiv1"
	"cloud.google.com/go/artifactregistry/apiv1/artifactregistrypb"
	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

func DownloadCredentialsFile() error {
	cmd := exec.Command("bash", "-c", `curl --silent "https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/download-secure-files/-/raw/main/installer" | bash`)

	// Run the command
	err := cmd.Run()
	if err != nil {
		return fmt.Errorf("Error running script: %v\n", err)
	}

	fmt.Println("Secure files are downloaded")
	return nil
}

// SetupApplicationDefaultCredential composes the credential file used to authenticate Google API calls.
// The file name is composed by gitlabCIJobId; the file content is built on oidcJwt and Workload Identity Federation Provider provided by Gitlab CI.
// If serviceAccount is provided, the service Account impersonation is applied during authentication.
//
// See ADC details in: https://cloud.google.com/docs/authentication/application-default-credentials#GAC.
// See Gitlab Workload Identity Federation details in: https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/.
func SetupApplicationDefaultCredential(gitlabCIJobId, oidcJwt, wip, serviceAccount string) error {
	// read jwt token input, write it to a JWT file
	jwtFile, err := createJWTFile(gitlabCIJobId, oidcJwt)
	if err != nil {
		return err
	}

	// compose the credential file, inlcuding JWT file
	credentialFile, err := createCredentialFile(gitlabCIJobId, jwtFile, wip, serviceAccount)
	if err != nil {
		return err
	}

	fmt.Printf("auth completed, file: %s", credentialFile)
	return nil
}

func createJWTFile(gitlabCIJobId, oidcJwt string) (string, error) {
	// Create file
	path := fmt.Sprintf("/tmp/oidc-jwt-%s.txt", gitlabCIJobId)
	file, err := os.Create(path)
	if err != nil {
		return "", err
	}
	defer func() {
		if err := file.Close(); err != nil {
			fmt.Println("Error closing file:", err)
		}
	}()

	// Write data to file
	data := []byte(oidcJwt)
	_, err = file.Write(data)
	if err != nil {
		return "", err
	}

	return path, err
}

func createCredentialFile(ciId, jwtFilePath, wip, serviceAccount string) (string, error) {
	config := ExternalAccountConfig{
		Type:             "external_account",
		Audience:         wip,
		SubjectTokenType: "urn:ietf:params:oauth:token-type:jwt",
		TokenURL:         "https://sts.googleapis.com/v1/token",
		CredentialSource: CredentialSource{
			File: jwtFilePath,
			Format: Format{
				Type: "text",
			},
		},
	}
	if serviceAccount != "" {
		fmt.Printf("service account provided, authenticating use Workload Identity Federation with Service Account impersonation...")
		config.ServiceAccountImpersonationURL = fmt.Sprintf("https://iamcredentials.googleapis.com/v1/projects/-/serviceAccounts/%s:generateAccessToken", serviceAccount)
	}

	// Convert the struct to JSON and write to file
	jsonBytes, err := json.Marshal(config)
	if err != nil {
		return "", nil
	}
	path := fmt.Sprintf("/tmp/oidc-credential-%s.json", ciId)
	err = ioutil.WriteFile(path, jsonBytes, 0644)
	if err != nil {
		return "", nil
	}

	projectId := "gitlab-zhangquan"
	ctx := context.Background()
	/*
	credentials, err := google.FindDefaultCredentials(ctx, "https://www.googleapis.com/auth/cloud-platform")
	if err != nil {
		return "", fmt.Errorf("failed to generate default credentials: %w", err)
	}*/
	client, err := storage.NewClient(ctx, option.WithCredentialsFile(path))
	if err != nil {
		return "",fmt.Errorf("NewClient: %w", err)
	}
	defer client.Close()
	fmt.Println("!!!! list buckets")
	it := client.Buckets(ctx, projectId)
	for {
		bucketAttrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return "",err
		}
		fmt.Println("Bucket: %v\n", bucketAttrs.Name)
	}



	fmt.Println("!!!!ar client")
	//audience := "https://artifactregistry.googleapis.com/v1/projects/gitlab-zhangquan/locations/us-central1/repositories" // Update with your API endpoint
	client2, err :=artifactregistry.NewClient(ctx, option.WithCredentialsFile(path))
	fmt.Println("!!!!get client")
	if err != nil {
		return "",err
	}
	fmt.Println("!!!!before list")
	it2:=client2.ListRepositories(ctx,&artifactregistrypb.ListRepositoriesRequest{Parent: "projects/gitlab-zhangquan/locations/us-central1"})
	for {
		resp, err := it2.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return "",err
		}

		fmt.Println("!!!resp",resp)
	}


	// Construct the GoogleCredentials object which obtains the default configuration from your
	// working environment.
	/*
	scope := "https://www.googleapis.com/auth/cloud-platform"
	credentials, err := google.FindDefaultCredentials(ctx, scope)
	if err != nil {
					return "",fmt.Errorf("failed to generate default credentials: %w", err)
	}

	ts, err := impersonate.CredentialsTokenSource(ctx, impersonate.CredentialsConfig{
					TargetPrincipal: "yongxuanzhang-gitlab@gitlab-zhangquan.iam.gserviceaccount.com",
					Scopes:          []string{scope},
					Lifetime:        300 * time.Second,
					// delegates: The chained list of delegates required to grant the final accessToken.
					// For more information, see:
					// https://cloud.google.com/iam/docs/create-short-lived-credentials-direct#sa-credentials-permissions
					// Delegates is NOT USED here.
					Delegates: []string{},
	}, option.WithCredentials(credentials))
	if err != nil {
					return "",fmt.Errorf("CredentialsTokenSource error: %w", err)
	}
	t, err := ts.Token()
	if err != nil {
					return "",fmt.Errorf("failed to receive token: %w", err)
	}

	fmt.Println("Access Token:", t.AccessToken)

	tokenPath := fmt.Sprintf("token.txt")
	tokenfile, err := os.Create(tokenPath)
	if err != nil {
		return "", err
	}
	defer func() {
		if err := tokenfile.Close(); err != nil {
			fmt.Println("Error closing file:", err)
		}
	}()

	// Write data to file
	data := []byte(t.AccessToken)
	_, err = tokenfile.Write(data)
	if err != nil {
		return "", err
	}*/

	return path, nil
}
