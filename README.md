# google-cloud-auth

A CLI command tool to generate authencitation files for GCloud or Cloud Client Library

## Usage
Embeded the `google-cloud-auth` image to your specific Gitlab Pipeline/Components and run `google-cloud-auth auth ...` commands 
to generate Workload Identity Federation credential file or download user-provided credential files to authenticate requests to GCP via [GCloud][gcloud] or [Google Cloud Client Libraries][cloud-client-lib].

### Workload Identity Federation (Preferred)
The generated credential file locates at `/tmp/oidc-jwt-{ci_job_id}.json`.

To authenticate with GCloud[gcloud], you need to set [`GOOGLE_APPLICATION_CREDENTIALS`][lib-auth] to the generated credential file path.

To authenticate with Google Client Library[cloud-client-lib], you need to set `GCLOUDSDK_AUTH_CREDENTIAL_FILE_OVERRIDE` to the generated credential file path.


### Credentials File
User can provide credentials files (e.g. Service Account private key file) by uploading them to [Gitlab Secure File][secure-file]. The binary will fetch the secure file from Gitlab when using this authentication method.

See example usage in the [Google Cloud Deploy Gitlab Components][cloud-deploy]

## Inputs
### Inputs: Workload Identity Federation
The following inputs are for _authenticating_ to Google Cloud via Workload
Identity Federation.

-   `workload-identity-provider`: (Required) The full identifier of the Workload
    Identity Provider, including the project number, pool name, and provider
    name. If provided, this must be the full identifier which includes all
    parts:

    ```text
    projects/123456789/locations/global/workloadIdentityPools/my-pool/providers/my-provider
    ```

    **Note**: `workload-identity-provider` cannot coexist with `credentials-file`.


-   `gcp-oidc-jwt`: (Required) The full OIDC JWT provided by Gitlab, can be found as `id_tokens.GCP_OIDC_JWT` in the
     Gitlab CI/CD config.

    ```text
    id_tokens:
        GCP_OIDC_JWT:
        aud: ...
    ```

-   `service-account`: (Optional) Email address or unique identifier of the
    Google Cloud service account for which to impersonate and generate
    credentials. For example:

    ```text
    my-service-account@my-project.iam.gserviceaccount.com
    ```

    Without this input, the Gitlab Components using this binary will use Direct Workload Identity
    Federation. If this input is provided, the Gitlab Components will use
    Workload Identity Federation through a Service Account.

### Inputs: Credential Files
-   `credentials-file`: (Required) The name of the credential file that user uploaded via [Gitlab Secure File][secure-file].

    **Note**: `credentials-file` cannot coexist with `workload-identity-provider`.


### Inputs: Miscellaneous

- `ci-job-id`: (Optional) The ID of the CI job running the binary, used to generate Workload Identity Federation based credential file paths.

[secure-file]: https://docs.gitlab.com/ee/ci/secure_files/
[cloud-client-lib]: https://cloud.google.com/apis/docs/cloud-client-libraries
[gcloud]: https://cloud.google.com/sdk?hl=en
[cloud-deploy]: https://gitlab.com/quanzhang/my-component/-/blob/main/templates/cloud-deploy-img.yml?ref_type=6a8ad3e2697d1a01a9d27f092f05c5c3099ab405
[lib-auth]: https://cloud.google.com/docs/authentication/application-default-credentials
